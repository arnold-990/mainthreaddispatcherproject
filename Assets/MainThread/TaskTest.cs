﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class TaskTest : MonoBehaviour
{
    public Transform Cube;
    private void Start()
    {
        RotateCube();
    }
    private async void RotateCube()
    {
        Task newTask = Task.Run(Count);
        Task rotationTask = Task.Run(() => Rotate360(Vector3.up));
        await rotationTask;
        Task rotationTask2 = Task.Run(() => Rotate360(Vector3.right));
        await rotationTask2;
        Task rotationTask3 = Task.Run(() => Rotate360(Vector3.forward));
        await rotationTask3;
    }
    public void Rotate360(Vector3 rotate)
    {
        for (int i = 0; i < 36; i++) 
        {
            Debug.Log(i);
            MainThreadDispatcher.Instance.InvokeIsMainThread(() =>
            { 
                Cube.transform.Rotate(rotate, 10);
            });
            Thread.Sleep(100);
        }
    }
    private void Count()
    {
        MainThreadDispatcher.Instance.InvokeIsMainThread(() =>
        {
            Debug.Log("Invoke this in main thread 1 ");
        });
        Thread.Sleep(1000);
        MainThreadDispatcher.Instance.InvokeIsMainThread(() =>
        {
            Debug.Log("Invoke this in main thread 2");
        });
        Thread.Sleep(1000);
        MainThreadDispatcher.Instance.InvokeIsMainThread(() =>
        {
            Debug.Log("Invoke this in main thread 3 ");
        });
    }
}