﻿using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class NewTcpClient : MonoBehaviour
{
    public int port;
    public string ip;

    public string TestMessage = "Hello";

    private TcpClient tcpClient;

    private Task clientListenTask;

    // Start is called before the first frame update
    void Start()
    {
        clientListenTask = Task.Run(Listen);
    }

    private void Listen()
    {
        tcpClient = new TcpClient(ip, port);
        MainThreadDispatcher.Instance.InvokeIsMainThread(()=>
        {
            Debug.Log(tcpClient);
        });
    }

    [ContextMenu("Test")]
    public void SendMsg()
    {
        if (tcpClient == null)
        {
            return;
        }

        NetworkStream stream = tcpClient.GetStream();

        // Convert string message to byte array.                 
        byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(TestMessage);
        // Write byte array to socketConnection stream.                 
        stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
    }
}