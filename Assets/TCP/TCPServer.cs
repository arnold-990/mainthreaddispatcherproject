﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class TCPServer : MonoBehaviour
{
    [SerializeField]
    private int Port = 13000;
    private TcpListener tcpListener;
    private Task tcpListnerTask;
    private TcpClient connectedTCPClient;
    private void Start()
    {
        tcpListnerTask = Task.Run(() => { ListenForIncomingRequest(); });
    }

    private void ListenForIncomingRequest()
    {
        try
        {
            tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();
            MainThreadDispatcher.Instance.InvokeIsMainThread(() => { Debug.Log("Server is listning"); });
            byte[] bytes = new byte[1024];
            while(true)
            {
                using(connectedTCPClient = tcpListener.AcceptTcpClient())
                {
                    using (NetworkStream stream = connectedTCPClient.GetStream())
                    {
                        int length;
                        while((length = stream.Read(bytes,0,bytes.Length)) != 0)
                        {
                            var inComingData = new byte[length];
                            Array.Copy(bytes, 0, inComingData, 0, length);
                            string clientMessage = Encoding.ASCII.GetString(inComingData);
                            Debug.Log("client message received: " + clientMessage);
                        }
                    }
                }
            }
        }
        catch(SocketException socketExeption)
        {
            Debug.Log("SocketException " + socketExeption.ToString());
        }
    }
    private void SendMessage()
    {
        if(connectedTCPClient == null)
        {
            return;
        }
        try
        {
            NetworkStream stream = connectedTCPClient.GetStream();
            if(stream.CanWrite)
            {
                string serverMessage = "This is a message from your server. ";
                byte[] serverMessageByteArray = Encoding.ASCII.GetBytes(serverMessage);
                stream.Write(serverMessageByteArray, 0, serverMessage.Length);
                Debug.Log("Server sent his message - should be received by client");
            }
        }
        catch(SocketException socketExeption)
        {
            Debug.Log("SocketException " + socketExeption);
        }
    }
}